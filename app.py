# This is a flask web server
# http://flask.pocoo.org/docs/quickstart/#quickstart
#
# /api/v1/extract extracts the text info using goose extractor
#
# /api/v1/train analyzes text features with the dataset

from flask import Flask, request, render_template,jsonify
from goose import Goose
from sklearn.feature_extraction.text import TfidfVectorizer
import json
 
app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/api/v1/feature',methods=['POST'])
def feature():
    text = request.form['text']
    dataset = request.form['dataset']
    result = json.loads(dataset)
    response = {'text' : text , 'dataset' : dataset}
    # # return jsonify(response)
    vectorizer = TfidfVectorizer(min_df = 1, stop_words = 'english', analyzer = 'word')
    tfidf = vectorizer.fit_transform(result)
    print vectorizer.get_feature_names()
    smatrix = vectorizer.transform(text)
    print smatrix.todense()
    return jsonify(response) 

 
@app.route('/api/v1/extract')
def extract():
    url = request.args.get('url')
    g = Goose()
    article = g.extract(url=url)
    text = article.cleaned_text.replace("\n\n"," ")
    text = text.replace("&"," ")
    if not article.top_image:
    	response = {'title' : article.title , 'text' : text, 'image': ""}
    else:
        response = {'title' : article.title , 'text' : text,'image': article.top_image.src}
    return jsonify(response)
 
if __name__ == "__main__":
    app.run(debug=True)
